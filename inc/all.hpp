#ifndef __DEFINE__
#define __DEFINE__

#include <mpi.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <cstdlib>
#include <algorithm>
#include "mkl.h"
#include <float.h>
#include <math.h>
#include <iomanip>
#include "arrays.hpp"
#include "structs.hpp"
#include "BelosConfigDefs.hpp"
#include "BelosLinearProblem.hpp"
#include "BelosEpetraAdapter.hpp"
#include "BelosPseudoBlockGmresSolMgr.hpp"
#include "BelosBlockGmresSolMgr.hpp"
#include "BelosBlockCGSolMgr.hpp"
#include "BelosPseudoBlockCGSolMgr.hpp"
#include "Ifpack.h"
#include "Ifpack_AdditiveSchwarz.h"
#include "Ifpack_PointRelaxation.h"
#include "Ifpack_BlockRelaxation.h"
#include "Ifpack_SparseContainer.h"
#include "Ifpack_ILU.h"
#include "Ifpack_Amesos.h"
#include "AztecOO.h"
#include "Teuchos_CommandLineProcessor.hpp"
#include "Teuchos_ParameterList.hpp"
#include "Teuchos_XMLParameterListHelpers.hpp"
#include "Teuchos_oblackholestream.hpp"
#include "Teuchos_StandardCatchMacros.hpp"
#include "Teuchos_GlobalMPISession.hpp"
#include "Teuchos_DefaultMpiComm.hpp"
#include <Tpetra_Version.hpp>
#include "Teuchos_Array.hpp"
#include "Teuchos_ArrayViewDecl.hpp"
#include "Teuchos_TimeMonitor.hpp"
#include <Teuchos_Flops.hpp>
#include <Tpetra_Util.hpp>
#include "EpetraExt_HDF5.h"
#include "EpetraExt_Transpose_RowMatrix.h"
#include "Trilinos_Util_CommandLineParser.h"
#include "BelosGCRODRSolMgr.hpp"
#include "Amesos_ConfigDefs.h"
#include "Amesos.h"
#include <Isorropia_Exception.hpp>
#include <Isorropia_Epetra.hpp>
#include <Isorropia_EpetraRedistributor.hpp>
#include <Isorropia_EpetraPartitioner.hpp>
#include <Isorropia_EpetraCostDescriber.hpp>
#include <Tpetra_Map.hpp>
#include <Tpetra_Map_decl.hpp>
#include <Tpetra_CrsMatrix.hpp>
#include <Tpetra_Vector.hpp>
#include "Tpetra_Vector_def.hpp"
#include <Tpetra_MultiVector.hpp>
#include "Tpetra_MultiVector_def.hpp"
#include <ml_MultiLevelPreconditioner.h>
class TableHelp
{
public:
	TableHelp()
	:pagewidth_(80)
	{}
	void setpagewidth(int pw) const {pagewidth_ = pw;}
	std::string line() const;
	std::string bigline() const;
	std::string starline() const;
private:
	mutable int pagewidth_;
};
#endif

