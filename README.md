# Sample 3 #

A parallel 3D anisotropic Laplace solver to compute flow of current in a domain of interest; domain is a label file in ascii format. Uses FEM method to solve the equation with H-grad elements on the label of interest. An AMG (V-cycle) preconditioned CG is the solver.

### Dependencies ###
* Usual suspects (refer to previous samples)