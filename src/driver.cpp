#include "all.hpp"

#include "Teuchos_DefaultComm.hpp"
#include "Teuchos_ArrayView.hpp"
#include "Intrepid_ArrayTools.hpp"
#include "Intrepid_FunctionSpaceTools.hpp"
#include "Intrepid_FieldContainer.hpp"
#include "Intrepid_CellTools.hpp"
#include "Intrepid_HGRAD_HEX_C1_FEM.hpp"
#include "Intrepid_RealSpaceTools.hpp"
#include "Intrepid_DefaultCubatureFactory.hpp"
#include "Intrepid_Utils.hpp"
#include <Intrepid_HGRAD_QUAD_C1_FEM.hpp>
#include "Shards_CellTopology.hpp"
#include "MueLu.hpp"
#include "MueLu_FactoryManager.hpp"
#include "MueLu_DirectSolver.hpp"
#include "MueLu_Hierarchy.hpp"
#include "MueLu_SaPFactory.hpp"
#include "MueLu_GenericRFactory.hpp"
#include "MueLu_RAPFactory.hpp"
#include "MueLu_TentativePFactory.hpp"
#include "MueLu_Ifpack2Smoother.hpp"
#include "MueLu_SmootherFactory.hpp"
#include "MueLu_RigidBodyModeFactory.hpp"
#include <MueLu_UseDefaultTypes.hpp>
#include <MueLu_MLParameterListInterpreter.hpp>
#include "MueLu_Amesos2Smoother.hpp"
#include <BelosConfigDefs.hpp>
#include <BelosLinearProblem.hpp>
#include <BelosBlockCGSolMgr.hpp>
#include <BelosXpetraAdapter.hpp>
#include <BelosMueLuAdapter.hpp>
#include <MueLu_UseShortNames.hpp>

using Teuchos::ParameterList;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::TimeMonitor;
using Teuchos::Array;
using Teuchos::Time;
using Teuchos::TableFormat;
using namespace Intrepid;
typedef double ST;
typedef long long int LLT;
typedef Kokkos::DefaultNode::DefaultNodeType NO;
typedef Tpetra::CrsMatrix<ST, LO, LLT, NO, LMO> matrix_type;
typedef Tpetra::Vector<ST, LO, LLT, NO> vector_type;

typedef Tpetra::global_size_t GST;                
typedef Tpetra::Map<LO, LLT, NO> map_type;
typedef Tpetra::Export<LO, LLT, NO> export_type;
typedef Tpetra::Import<LO, LLT, NO> import_type;

/***********************************************************************************/
/*		Some global variables for the mesh				   */
/***********************************************************************************/

int phase, nx, ny, nz, perhost, grow, max_levels, eig_ratio, coarse_sweep, sweeps0, sweeps1, sweeps2, nslices;
bool exported;
double eps, coarse_alpha;
std::string fname("141_1_B_xy390_z515.bin");
bool check_symmetry;
RCP<TableHelp> draw = rcp(new TableHelp);
std::string smoother0, smoother1, smoother2, coarse_solve, agg_method;
LLT gnode_start=0, gnode_end=0;

/***********************************************************************************/
/*		Set up the Matrix type and insert elements			  */
/***********************************************************************************/


std::string TableHelp::line() const
{
        std::ostringstream oss;
        for(int i = 0; i < pagewidth_; i++)
        {
                oss << "-";
        }
        return oss.str();
}

std::string TableHelp::bigline() const
{
        std::ostringstream oss;
        for(int i = 0; i < pagewidth_; i++)
        {
                oss << "=";
        }
        return oss.str();
}

std::string TableHelp::starline() const
{
        std::ostringstream oss;
        for(int i = 0; i < pagewidth_; i++)
        {
                oss << "*";
        }
        return oss.str();
}

bool inline neighbors(const MAT3D<unsigned char> & image, const int & z, const int & y, const int & x)
{
int startx, starty, startz;
int endx, endy, endz;
bool out;
	startx = (x == 0)?0:-1;
	endx   = (x == image.sizex-1)?0:1;
	starty = (y == 0)?0:-1;
        endy   = (y == image.sizey-1)?0:1;
	startz = (z == 0)?0:-1;
        endz   = (z == image.sizez-1)?0:1;


	for(int i = startz; i <= endz; i++)
		for(int j = starty; j <= endy; j++)
			for(int k = startx; k <= endx; k++)
			{
				if (image.data[z+i][y+j][x+k] != phase)
					out = true;
				else
					out = false;
			}	 
	return out;
}


bool inline hex_neighbors(const MAT3D<unsigned char> & image, const MAT3D<unsigned char> & comp_node, const int & i, const int & j, const int & k)
{
bool out;

	if ((image.data[i][j][k+1] ==phase) && (image.data[i][j+1][k+1] ==phase) && (image.data[i][j+1][k] ==phase)
		&& (image.data[i+1][j][k] ==phase) && (image.data[i+1][j][k+1] ==phase) && (image.data[i+1][j+1][k+1] ==phase)
		&& (image.data[i+1][j+1][k] ==phase))
	{
		out = true;
		comp_node.data[i][j][k] = 1; comp_node.data[i][j][k+1] =1; comp_node.data[i][j+1][k+1] =1; comp_node.data[i][j+1][k] =1;
		comp_node.data[i+1][j][k] = 1; comp_node.data[i+1][j][k+1] =1; comp_node.data[i+1][j+1][k+1] =1; comp_node.data[i+1][j+1][k] =1;
	}
	else
		out = false;



	return out;
}


bool inline hex_neighbors(const MAT3D<unsigned char> & image, const int & i, const int & j, const int & k)
{
bool out;
        if ((image.data[i][j][k+1] ==phase) && (image.data[i][j+1][k+1] ==phase) && (image.data[i][j+1][k] ==phase)
                && (image.data[i+1][j][k] ==phase) && (image.data[i+1][j][k+1] ==phase) && (image.data[i+1][j+1][k+1] ==phase)
                && (image.data[i+1][j+1][k] ==phase))
                out = true;
        else
                out = false;

        return out;
}

void inline node_coordinates(FieldContainer<unsigned short> & node_coord, const LLT & elem_counter, const int & i, const int & j, const int & k)
{
	node_coord(elem_counter,0,0) = k; node_coord(elem_counter,0,1) = j; node_coord(elem_counter,0,2) = i;
	node_coord(elem_counter,1,0) = k+1; node_coord(elem_counter,1,1) = j; node_coord(elem_counter,1,2) = i;
	node_coord(elem_counter,2,0) = k+1; node_coord(elem_counter,2,1) = j+1; node_coord(elem_counter,2,2) = i;
	node_coord(elem_counter,3,0) = k; node_coord(elem_counter,3,1) = j+1; node_coord(elem_counter,3,2) = i;
	node_coord(elem_counter,4,0) = k; node_coord(elem_counter,4,1) = j; node_coord(elem_counter,4,2) = i+1;
	node_coord(elem_counter,5,0) = k+1; node_coord(elem_counter,5,1) = j; node_coord(elem_counter,5,2) = i+1;
	node_coord(elem_counter,6,0) = k+1; node_coord(elem_counter,6, 1) = j+1; node_coord(elem_counter,6,2) = i+1;
	node_coord(elem_counter,7,0) = k; node_coord(elem_counter,7, 1) = j+1; node_coord(elem_counter,7,2) = i+1;
}

void make_nodes(Teuchos::RCP<vector_type> & x, Teuchos::RCP<const Teuchos::Comm<int> >& comm, std::ostream & out)
{

	using Teuchos::TimeMonitor;

	/***********************************************************************************/
	/*		Get time monitor counters for various processes			   */
	/***********************************************************************************/
	Teuchos::RCP<Teuchos::Time> io_timer  = TimeMonitor::getNewCounter ("I/O");
	Teuchos::RCP<Teuchos::Time> nn_timer  = TimeMonitor::getNewCounter ("Node numbering");
	Teuchos::RCP<Teuchos::Time> assembly_timer  = TimeMonitor::getNewCounter ("FEM assembly");


        Teuchos::RCP<NO> node = Kokkos::DefaultNode::getDefaultNode ();
	const LLT indexBase = 0;

	/***********************************************************************************/
	/*		Create subcommunicator and a bool variable to access it		   */
	/***********************************************************************************/
	
	const int rank = comm->getRank();
        Teuchos::Array<int> tprocs(ceil(1.*comm->getSize()/perhost)), procs(ceil(1.*comm->getSize()/perhost));
        if ((rank%perhost) == 0)
                tprocs[rank/perhost] = rank;
        Teuchos::reduceAll<int, int>(*comm, Teuchos::REDUCE_SUM, procs.size(), &tprocs[0], &procs[0]);
        Teuchos::RCP<Teuchos::Comm<int> > subcomm = comm->createSubcommunicator(procs);

	/***********************************************************************************/
	/*		Create a bool variable to test if proc in sub-communicator	  */
	/***********************************************************************************/

	bool check = false; 
        if(std::find(procs.begin(), procs.end(), rank) != procs.end())
                check = true;
        else
                check = false;

	LLT numGNodes;
	
	/***********************************************************************************/
	/*	Create a map on sub-communicator for to compute local elements and nodes   */
	/***********************************************************************************/
	// subNMap is the node map and subEMap is the element Map;

        Teuchos::RCP<const map_type> subNMap, subEMap;
	size_t num_local_nodes = 0, num_local_elements = 1;
	
	out << draw->bigline() << std::endl;

	/***********************************************************************************/
	/*		Cubature on Hex elements- get integration points and weights	   */
	/***********************************************************************************/
	
	typedef shards::CellTopology    CellTopology;
	CellTopology h8(shards::getCellTopologyData<shards::Hexahedron<8> >() );

	// Get dimensions 

	int numNodesPerElem = h8.getNodeCount();
	int spaceDim = h8.getDimension();

	DefaultCubatureFactory<double>  cubFactory;
	int cubDegree = 3;
	Teuchos::RCP<Cubature<double> > hexCub = cubFactory.create(h8, cubDegree);

	int cubDim       = hexCub->getDimension();
	int numCubPoints = hexCub->getNumPoints();

	FieldContainer<double> cubPoints(numCubPoints, cubDim);
	FieldContainer<double> cubWeights(numCubPoints);

	hexCub->getCubature(cubPoints, cubWeights);


	/***********************************************************************************/
	/*  Get basis on Hex elements and evaluate basis and gradients at cubature points  */
	/***********************************************************************************/
	
	Basis_HGRAD_HEX_C1_FEM<double, FieldContainer<double> > hexHGradBasis;
	int numFieldsG = hexHGradBasis.getCardinality();

	FieldContainer<double> hexGrads(numFieldsG, numCubPoints, spaceDim);

	// Evaluate basis values and gradients at cubature points
	hexHGradBasis.getValues(hexGrads, cubPoints, OPERATOR_GRAD);

	// Settings and data structures for mass and stiffness matrices
	typedef CellTools<double>  CellTools;
	typedef FunctionSpaceTools fst;
	int numCells = 1;

	FieldContainer<double> hexNodes(numCells, numNodesPerElem, spaceDim);
	FieldContainer<double> hexJacobian(numCells, numCubPoints, spaceDim, spaceDim);
	FieldContainer<double> hexJacobInv(numCells, numCubPoints, spaceDim, spaceDim);
	FieldContainer<double> hexJacobDet(numCells, numCubPoints);

	// Containers for element HGRAD stiffness matrix

	FieldContainer<double> localStiffMatrix(numCells, numFieldsG, numFieldsG);
	FieldContainer<double> weightedMeasure(numCells, numCubPoints);
	FieldContainer<double> hexGradsTransformed(numCells, numFieldsG, numCubPoints, spaceDim);
	FieldContainer<double> hexGradsTransformedWeighted(numCells, numFieldsG, numCubPoints, spaceDim);

	// Container for cubature points in physical space
	FieldContainer<double> physCubPoints(numCells, numCubPoints, cubDim);	
	LLT vox_count, bvox, ivox, numGElem, cvox;
	vox_count = bvox = ivox = numGElem = cvox = 0;

	FieldContainer<LLT> elem_node(num_local_elements,3);
	FieldContainer<unsigned short> node_coord(num_local_elements, 8, 3);
	LLT lbc_node = 0, ubc_node = 0;
	num_local_elements = 0;
        MAT3D<unsigned char> comp_node(nx, ny, nz);
	if (check)
	{

		MAT3D<unsigned char> image (nx, ny, nz);
        	MAT3D<LLT> node_number(image.sizex, image.sizey, image.sizez);
		std::fill(comp_node.space, comp_node.space+comp_node.sizex*comp_node.sizey*comp_node.sizez, 0);
		std::fill(node_number.space, node_number.space+node_number.sizex*node_number.sizey*node_number.sizez, 0);


		MAT1D<LLT> nel(nz-1);
		std::fill(nel.data, nel.data+nel.sizex, 0.);
		{
			TimeMonitor io(*io_timer);
			std::fstream f1;
			f1.open(fname.c_str(), std::ios::in | std::ios::binary);	
			f1.read(reinterpret_cast<char*>(&image.data[0][0][0]), sizeof(unsigned char)*image.sizex*image.sizey*image.sizez);
			f1.close();
		} // end of io_timer local scope

		out << std::endl;	
		out << "\tFinished reading file " << fname << "...computing voxel counts " << std::endl << std::endl;
	

		{
			TimeMonitor nn(*nn_timer);

			//Compute total number of elements

			for(int i = 0; i < nz-1; i++)
			{
				for(int j = 0; j < ny-1; j++)
					for(int k = 0; k < nx-1; k++)
						if (image.data[i][j][k] == phase)
						{
							if (hex_neighbors(image, comp_node, i, j, k))
								numGElem++;
						}
			       nel.data[i] = numGElem;
			}

// Parts of the loop below, like counting computational voxels,  can be pushed inside hex_neighbors loop. 
// However, I am a little lazy and I find the loop seperation below more logical in terms of thought process.


			for(int i = 0; i < nz; i++)
				for(int j = 0; j < ny; j++)
					for(int k = 0; k < nx; k++)
					{
						if(image.data[i][j][k] == phase)
						{
							vox_count++;
							if (comp_node.data[i][j][k] == 1)
							{
								node_number.data[i][j][k] = cvox;
								if (i == 0)
									lbc_node++;
								if (i < nz-1)
									ubc_node++;
								if (i < (nz/2-nslices/2))
									gnode_start++;
								if (i < (nz/2+nslices/2))
									gnode_end++;
//bvox metric is rather strange...for example at the top and bottom slice where Dirichlet BC is imposed no need to check for 26 neighbor connectivity.
								cvox++;
							}
						}
					}
					
			// Go through each element to assign global node numbers of the nodes making up the element
									
			out << std::fixed <<"\tTotal phase " << phase << " voxels are                    : " << vox_count << std::endl;
			out << "\tTotal computational voxels are 	            : " << cvox << std::endl;


			out << "\tNumber of Hex elements are 	            : " << numGElem << std::endl;

			/***********************************************************************************/
			/*	Uncomment next two lines to identify number of dirichlet nodes in the      */
			/*	bottom slice and th enode on which the top slice starts			   */
			/***********************************************************************************/

			numGNodes = cvox;
			subNMap = rcp (new map_type (numGNodes, indexBase, subcomm, Tpetra::GloballyDistributed, node));
			subEMap = rcp (new map_type (numGElem, indexBase, subcomm, Tpetra::GloballyDistributed, node));
			num_local_nodes = subNMap->getNodeNumElements();
			num_local_elements = subEMap->getNodeNumElements();
			
			LLT elem_start = subEMap->getMinGlobalIndex();
			LLT elem_end = subEMap->getMaxGlobalIndex();
			
			LLT elem_counter = 0, temp_elem = 0;

			
			elem_node.resize(num_local_elements, 8);
			node_coord.resize(num_local_elements, 8, 3);

			for(int i = 0; i < image.sizez-1; i++)
				for(int j = 0; j < image.sizey-1; j++)
					for(int k = 0; k < image.sizex-1; k++)
						if (comp_node.data[i][j][k] == 1)
						{
							if (elem_counter < num_local_elements)
							{
								if (hex_neighbors(image, i, j, k))
								{
									if ((temp_elem >= elem_start) && (temp_elem <= elem_end))
									{
										elem_node(elem_counter,0) = node_number.data[i][j][k];
										elem_node(elem_counter,1) = node_number.data[i][j][k+1];
										elem_node(elem_counter,2) = node_number.data[i][j+1][k+1];
										elem_node(elem_counter,3) = node_number.data[i][j+1][k];
										elem_node(elem_counter,4) = node_number.data[i+1][j][k];
										elem_node(elem_counter,5) = node_number.data[i+1][j][k+1];
										elem_node(elem_counter,6) = node_number.data[i+1][j+1][k+1];
										elem_node(elem_counter,7) = node_number.data[i+1][j+1][k];

										node_coordinates(node_coord, elem_counter, i, j, k);	

										elem_counter++;
									}
									temp_elem++;
								}
							}
						}
		} //end of nn_timer local scope
	
	} //end of check


	Teuchos::broadcast<int, LLT>(*comm, 0, &numGNodes);
	Teuchos::broadcast<int, LLT>(*comm, 0, &lbc_node);
	Teuchos::broadcast<int, LLT>(*comm, 0, &ubc_node);
	Teuchos::broadcast<int, LLT>(*comm, 0, &gnode_start);
	Teuchos::broadcast<int, LLT>(*comm, 0, &gnode_end);
	
	Teuchos::RCP<const map_type> red_global_node_map = rcp(new map_type((LLT) -1, num_local_nodes, indexBase, comm, node));

	Teuchos::RCP<const map_type> red_global_element_map = rcp(new map_type((LLT) -1, num_local_elements, indexBase, comm, node));

	Teuchos::RCP<const map_type> global_node_map = rcp (new map_type (numGNodes, indexBase, comm, Tpetra::GloballyDistributed, node));
	
	LLT crow = 0, maxind = 0;
	Teuchos::RCP<matrix_type> B;
	{
		Teuchos::RCP<matrix_type> A = rcp(new matrix_type(red_global_node_map, 21));

		Teuchos::Array<ST> values(8);
		Teuchos::Array<LLT> cols(8);

		LLT rowIndex = 0, colIndex = 0;

		{
			TimeMonitor assembly(*assembly_timer);

			for(int i = 0; i < num_local_elements; i++)
			{
				for(int j = 0; j < numNodesPerElem; j++)
				{
					hexNodes(0,j,0) = node_coord(i,j,0);
					hexNodes(0,j,1) = node_coord(i,j,1);
					hexNodes(0,j,2) = node_coord(i,j,2);
				}

				// Compute cell Jacobians, their inverses and their determinants
				CellTools::setJacobian(hexJacobian, cubPoints, hexNodes, h8);
				CellTools::setJacobianInv(hexJacobInv, hexJacobian );
				CellTools::setJacobianDet(hexJacobDet, hexJacobian );
		
				/***********************************************************************************/
				/*		Compute element HGrad stiffness matrices			   */
				/***********************************************************************************/

				fst::HGRADtransformGRAD<double>(hexGradsTransformed, hexJacobInv, hexGrads);                                 	/// J^{-1}(grad u)
				fst::computeCellMeasure<double>(weightedMeasure, hexJacobDet, cubWeights);                                     	/// det(J)*w
				fst::multiplyMeasure<double>(hexGradsTransformedWeighted, weightedMeasure, hexGradsTransformed);              	/// J^{-1}(grad u)*det(J)*w
				// integrate to compute element stiffness matrix
				fst::integrate<double>(localStiffMatrix, hexGradsTransformed, hexGradsTransformedWeighted, COMP_CPP);        	/// J^{-1}(grad u)*det(J)*w*(J^{-1}(grad u))	

				for (int row = 0; row < numFieldsG; row++)
				{
					rowIndex = elem_node(i,row);
					for (int col = 0; col < numFieldsG; col++)
					{
						cols[col] = elem_node(i,col);
						values[col] = static_cast<ST> (localStiffMatrix(0, row, col));

					} // end of second loop over basis functions
					if ((hexNodes(0,row,2) == 0) || (hexNodes(0,row,2) == nz-1))
						std::fill(values.begin(), values.end(), static_cast<ST>(0.0));
					A->insertGlobalValues(rowIndex, cols, values);
				} //end of first loop over basis functions
			} // end of local element loop
		}  ///end of assembly timer

		crow = red_global_node_map->getGlobalElement(0);
		maxind = red_global_node_map->getMaxGlobalIndex();
		{
			if ((crow < lbc_node) || (maxind >= ubc_node))
			{ 
				for(int i = 0; i < num_local_nodes; i++)
				{
					crow = red_global_node_map->getGlobalElement(i);
					if ((crow < lbc_node) || (crow >= ubc_node))
					{
						A->replaceGlobalValues(crow, Teuchos::tuple<LLT>(crow), Teuchos::tuple<ST>(1.0)); 
					}
				}
			}
		}
		A->globalAssemble(); A->fillComplete();
		B = Teuchos::rcp(new matrix_type (global_node_map, 0));
		export_type exporter (red_global_node_map, global_node_map);
		import_type importer (red_global_node_map, global_node_map);
		B->doImport(*A, importer, Tpetra::INSERT);
		B->fillComplete();
		out << "\tFinished global setup " << std::endl;
	}
	Teuchos::RCP<vector_type> b;

	crow = global_node_map->getGlobalElement(0);		
	num_local_nodes = global_node_map->getNodeNumElements();
	maxind = global_node_map->getMaxGlobalIndex();
	{
		x = rcp(new vector_type (global_node_map));
		b = rcp(new vector_type (global_node_map));
		x->putScalar(0.0); b->putScalar(0.0);
		if ((crow < lbc_node) || (maxind > ubc_node))
                { 
                        for(int i = 0; i < num_local_nodes; i++)
                        {
                                crow = global_node_map->getGlobalElement(i);
                                if (crow < lbc_node)
                                	b->sumIntoGlobalValue(crow, 5.0);
				else if (crow >= ubc_node)
					b->replaceGlobalValue(crow, 2.0);	
                        }       
                }   
	}


	/***********************************************************************************/
	/*	Uncomment section below to output global row specified by grow		   */
	/***********************************************************************************/
/*
	Teuchos::Array<ST> crsvals;
	Teuchos::Array<LLT int> indices;	
	crsvals.resize(1); indices.resize(1);
	size_t NumEntries = 0;
	out << "outputting values of A" << std::endl;
	NumEntries = A->getNumEntriesInGlobalRow(grow);
        if (NumEntries != Teuchos::OrdinalTraits<size_t>::invalid())
        {
                indices.resize(NumEntries); crsvals.resize(NumEntries);
                A->getGlobalRowCopy (grow, indices(), crsvals(), NumEntries);
                for(int i = 0; i < indices.size(); i++)
                        std::cout << indices[i] << " " << std::setprecision(15) << crsvals[i] << "\n";
        }
	comm->barrier();
	out << "outputting values of B" << std::endl;
	NumEntries = B->getNumEntriesInGlobalRow(grow);	
	if (NumEntries != Teuchos::OrdinalTraits<size_t>::invalid())
	{
		indices.resize(NumEntries); crsvals.resize(NumEntries);
		B->getGlobalRowCopy (grow, indices(), crsvals(), NumEntries);
        	for(int i = 0; i < indices.size(); i++) 
			std::cout << indices[i] << " " << std::setprecision(15) << crsvals[i] << "\n";
	}
*/
	comm->barrier();
	Teuchos::RCP<Xpetra::CrsMatrix<ST, LO, LLT, NO, LMO> > mueluA_ = rcp(new Xpetra::TpetraCrsMatrix<ST, LO, LLT, NO, LMO>(B));
      	Teuchos::RCP<Xpetra::Matrix <ST, LO, LLT, NO, LMO> > mueluA = rcp(new Xpetra::CrsMatrixWrap<ST, LO, LLT, NO, LMO>(mueluA_));
/*
	if (check_symmetry == true)
        {
		Teuchos::RCP<vector_type> x1 = rcp(new vector_type (B->getDomainMap ()));
		Teuchos::RCP<vector_type> b1 = rcp(new vector_type (B->getRangeMap ()));
		Teuchos::RCP<vector_type> b2 = rcp(new vector_type (B->getRangeMap ()));
		x1->putScalar(1.0f);
		B->apply(*x1, *b1);
		b2->putScalar(0.0);
                B->apply(*x1, *b2, Teuchos::TRANS);
                b1->update(-1.0, *b2, 1.0);
		double norm = b1->norm2();
		if (norm < eps)
                	out << std::setprecision(12) << "\tMatrix is symmetric. ||Ax-A^Tx|| = " << norm << endl;
                out << draw->bigline() << std::endl;
	}
*/

	/***********************************************************************************/
	/*		Preconditioned CG with AMG (v-cycle) as preconditioner			   */
	/***********************************************************************************/

	Teuchos::ParameterList MLList;
	MLList.set("ML output",0);
	MLList.set("max levels", max_levels);
	MLList.set("aggregation: type (level 0)", "METIS");
	MLList.set("aggregation: type (level 1)", "METIS");
	MLList.set("aggregation: type (level 2)", "METIS");
	MLList.set("smoother: type (level 0)", smoother0);
        MLList.set("smoother: type (level 1)", smoother1);
        MLList.set("smoother: type (level 2)", smoother2);
	MLList.set("smoother: sweeps (level 0)", sweeps0);
	MLList.set("smoother: sweeps (level 1)", sweeps1);
	MLList.set("smoother: sweeps (level 2)", sweeps2);
	MLList.set("coarse: pre or post", "both");
	MLList.set("smoother: pre or post", "both");
	MLList.set("coarse: type",coarse_solve);
	MLList.set("aggregation: nodes per aggregate", 27);
        MLList.set("coarse: sweeps", coarse_sweep);
	MueLu::MLParameterListInterpreter<ST, LO, LLT, NO, LMO> mueLuFactory (MLList);
        RCP<MueLu::Hierarchy<ST, LO, LLT, NO, LMO> > H = mueLuFactory.CreateHierarchy();
        H->GetLevel(0)->Set("A", mueluA);
        mueLuFactory.SetupHierarchy(*H);

	out << draw->bigline() << std::endl;

	typedef Tpetra::MultiVector<ST, LO, LLT, NO> MV;
	typedef Belos::OperatorT<MV>                OP;

	RCP<OP> belosOp   = rcp(new Belos::XpetraOp<ST, LO, LLT, NO>(mueluA));
	RCP<OP> belosPrec = rcp(new Belos::MueLuOp<ST, LO, LLT, NO>(H));      
	RCP< Belos::LinearProblem<ST, MV, OP> > belosProblem = rcp(new Belos::LinearProblem<ST, MV, OP>(belosOp, x, b));
	belosProblem->setLeftPrec(belosPrec);

	if (belosProblem->setProblem() == false)
	{
		std::cerr << "ERROR:  Belos::Problem failed to set up correctly!" << std::endl;
		exit(EXIT_FAILURE);
	}


	int maxiters = 1000, frequency = 50;
	double tol = 1e-06;
	
	Teuchos::ParameterList belosList;
	belosList.set("Block Size", 1);
	belosList.set("Output Frequency", frequency);
	belosList.set("Maximum Iterations",    maxiters);
	belosList.set("Convergence Tolerance", tol); 
	belosList.set("Verbosity", Belos::Errors + Belos::Warnings + Belos::TimingDetails + Belos::StatusTestDetails);
	belosList.set("Output Style", Belos::Brief);
	RCP< Belos::SolverManager<ST, MV, OP> > solver = rcp(new Belos::BlockCGSolMgr<ST, MV, OP>(belosProblem, rcp(&belosList, false)));

	Belos::ReturnType ret = solver->solve();

	out << "Number of iterations performed is : " << solver->getNumIters() << std::endl;
        out << "Achieved tolerance is : " << solver->achievedTol() << std::endl;

	out << "Will be using nodes " << gnode_start << " to " << gnode_end << " for gradient calculation" << std::endl;

	std::vector<LLT> nodes;
	if (comm->getRank() == 0)
	{
		nodes.resize(gnode_end-gnode_start+1);
		for(int i = 0; i < nodes.size(); i++)
                	nodes[i] = i+gnode_start;
	}
	else
		nodes.resize(0);
		

	RCP<Teuchos::ArrayView <const LLT> > grad_nodes = rcp (new Teuchos::ArrayView<const LLT> (nodes));

	RCP<map_type> grad_map = rcp(new map_type((LLT) -1, *(grad_nodes), (LLT) 0, comm, node)); 

	export_type grad_export(global_node_map, grad_map);

	Teuchos::RCP<vector_type> x0 = rcp(new vector_type (grad_map));
	x0->doExport (*x, grad_export, Tpetra::INSERT);

	if (rank == 0)
        {
		int start = (nz/2-nslices/2), end = nz/2+nslices/2, cnt = 0, count = 0;
		Teuchos::ArrayRCP<ST> x0_data = x0->get1dViewNonConst();
		MAT3D<ST> solution(nx, ny, nslices), grad_solution(nx,ny,nslices-2);
		for(int i = start; i < end; i++, cnt++)
                        for(int j = 0; j < ny; j++)
                                for(int k = 0; k < nx; k++)     
                                {
                                        if (comp_node.data[i][j][k] == 1)
                                        {
                                                solution.data[cnt][j][k] = x0_data[count];
                                                count++;
                                        }
                                        else
                                                solution.data[cnt][j][k] = 0.;
                                }
		ST sum = 0;
		cnt = 1;
		for(int i = start+1; i < end-1; i++, cnt++)
		{
                        for(int j = 0; j < ny; j++)
                                for(int k = 0; k < nx; k++)
                                {
					if ((comp_node.data[i][j][k] == 1) && (comp_node.data[i+1][j][k] == 1) && (comp_node.data[i-1][j][k] == 1))
					{
						grad_solution.data[cnt-1][j][k] = (solution.data[cnt+1][j][k] - solution.data[cnt-1][j][k])/2.;
						sum += -1*grad_solution.data[cnt-1][j][k];
					}						
					else if ((comp_node.data[i][j][k] == 1) && (comp_node.data[i+1][j][k] == 1))
					{
						grad_solution.data[cnt-1][j][k] = (solution.data[cnt+1][j][k] - solution.data[cnt][j][k]);
						sum += -1*grad_solution.data[cnt-1][j][k];
					}
					else if ((comp_node.data[i][j][k] == 1) && (comp_node.data[i-1][j][k] == 1))
					{
						grad_solution.data[cnt-1][j][k] = (solution.data[cnt][j][k] - solution.data[cnt-1][j][k]);
						sum += -1*grad_solution.data[cnt-1][j][k];
					}
                                }
		}
		std::cout << "FF is " << (3./(sum/nslices))*((1.*nx*ny)/nz) << std::endl; //using nine because it is (bc1-bc2)/(sum/no_of_slices for computation))
		std::fstream f1;
  		f1.open("Solution.raw", std::ios::out | std::ios::binary);
		f1.write(reinterpret_cast<char*>(&solution.data[0][0][0]), sizeof(ST)*nx*ny*(solution.sizez));
              	f1.close(); 
	}
}

int main(int argc, char *argv[])
{
	using Teuchos::Time;
  	using Teuchos::TimeMonitor;
	Teuchos::CommandLineProcessor clp;
  
	phase = 1;
	clp.setOption("phase", &phase, "Phase on which the grid is to be constructed");

	nx = 390;
	clp.setOption("nx", &nx, "Number of grid points in X-direction");
	
	ny = 390;
	clp.setOption("ny", &ny, "Number of grid points in Y-direction");

	nz = 515;
	clp.setOption("nz", &nz, "Number of grid points in Z-direction");

	clp.setOption("fname", &fname, "File name of input file");

	perhost = 8;
	clp.setOption("perhost", &perhost, "Number of CPU's per core");

	check_symmetry = false;
	clp.setOption("check_symmetry","no_check_symmetry", &check_symmetry, "Check for symmetry of Operator (Matrix)");

	exported=true;
	clp.setOption("exported","original", &exported, "Operator to use in construction of Hierarchy");

	grow = 34696;
	clp.setOption("grow", &grow, "Global row to output");

	eps = 1e-10;
	clp.setOption("eps", &eps, "A small number of checking closeness of doubles etc");

	max_levels = 6;
	clp.setOption("max_levels", &max_levels, "Maximum number of MG levels");

	eig_ratio=10;
	clp.setOption("eig_ratio", &eig_ratio, "Ratio of max-to-min eigen values at coarse level");

	smoother0="symmetric Gauss-Seidel";
	clp.setOption("smoother0", &smoother0, "Smoother at level 0");

	smoother1="symmetric Gauss-Seidel";
	clp.setOption("smoother1", &smoother1, "Smoother at level 1");

	smoother2="symmetric Gauss-Seidel";
	clp.setOption("smoother2", &smoother2, "Smoother at level 2");

	sweeps0=3;
        clp.setOption("sweeps0", &sweeps0, "Smoother sweeps at level 0");

	sweeps1=3;
        clp.setOption("sweeps1", &sweeps1, "Smoother sweeps at level 1");

	sweeps2=3;
        clp.setOption("sweeps2", &sweeps2, "Smoother sweeps at level 2");

	coarse_solve="symmetric Gauss-Seidel";
	clp.setOption("coarse_solve", &coarse_solve, "Coarse solver");

	double coarse_alpha = 5.0;
        clp.setOption("coarse_alpha", &coarse_alpha, "Chebyshev alpha for coarse grid");

	coarse_sweep = 5;
        clp.setOption("coarse_sweep", &coarse_sweep, "Coarse sweeps for smoother (order for Chebyshev");	

	std::string agg_method("SA");
        clp.setOption("agg_method", &agg_method, "aggregation method");

	nslices=10;
	clp.setOption("nslices", &nslices, "Number of slices to output for visualization");

	Teuchos::CommandLineProcessor::EParseCommandLineReturn parseReturn = clp.parse(argc, argv);
  	if(parseReturn == Teuchos::CommandLineProcessor::PARSE_HELP_PRINTED) return 0;
  	if(parseReturn != Teuchos::CommandLineProcessor::PARSE_SUCCESSFUL) return 1;



        Teuchos::GlobalMPISession mpiSession(&argc, &argv, NULL);
	Teuchos::RCP< const Teuchos::Comm<int> > Comm = Teuchos::DefaultComm<int>::getComm();

        Teuchos::oblackholestream nullstream;
        std::ostream &out = (Comm->getRank() == 0)?std::cout:nullstream;

	typedef Kokkos::DefaultNode::DefaultNodeType NO;
  	Teuchos::RCP<NO> node = Kokkos::DefaultNode::getDefaultNode ();
	
	Teuchos::RCP<vector_type> x;

	make_nodes(x, Comm, out);
}
